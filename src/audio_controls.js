function fract(x){ 
    return x - Math.floor(x);
  };
  
  function packFloat(x) {
    var s = x > 0 ? 1 : -1;
    var e = Math.floor(Math.log2(s*x));
    var m = s*x/Math.pow(2, e);
    return [
      Math.floor(fract((m-1)*256*256)*256),
      Math.floor(fract((m-1)*256)*256),
      Math.floor(fract((m-1)*1)*256),
      ((e+63) + (x>0?128:0))];
  } 
  const createWave = (l,s,arr) => {
    // Create WAVE header
    var headerLen = 46;
    var l1 = headerLen + l * 8 - 8;
    var l2 = l1 - 36;
    var wave = new Uint8Array(headerLen + l * 8);
    wave.set([
        82, //"r"
        73, //"i"
        70, //"f"
        70, //"f"
        l1 & 255,
        (l1 >> 8) & 255,
        (l1 >> 16) & 255,
        (l1 >> 24) & 255,
        87,  //"W"
        65,  //"A"
        86,  //"V"
        69,  //"E"
        102, //"f"
        109, //"m"
        116, //"t"
        32,  //" "
        18,  //"fmt " length
        0,   //
        0,   //
        0,   //
        3,   // PCM IEE FLOAT
        0,   // 
        2,   //channels
        0,   //
        68,  // sample rate 44100
        172,
        0,
        0,
        32,  // samplerate * bits per sample *channels/8
        98,
        5,
        0,
        8,  // bits per sample*channels/8
        0,
        32,  // Bits per sample
        0,
        0,  //padding
        0,  //padding
        100,  //"d"
        97,   //"a"
        116,  //"t"
        97,   //"a"
        l2 & 255,
        (l2 >> 8) & 255,
        (l2 >> 16) & 255,
        (l2 >> 24) & 255,
    ]);

    const arr_arr = new Uint8Array(arr.buffer);
    // Append actual wave data
    for (var i = 0, idx = headerLen; i < l*16; ++i) {
        // Note: We clamp here
        var y = arr_arr[s*4+i];
        wave[idx++]=y
        //var x =packFloat(y);
        //y = y < -32767 ? -32767 : y > 32767 ? 32767 : y;
        //wave[idx++] = x[0];
        //wave[idx++] = x[1];
        //wave[idx++] = x[2];
        //wave[idx++] = x[3];
    }

    // Return the WAVE formatted typed array
    return wave;
  };

  let audioEl = document.createElement("audio");
  let wave = createWave(l,s,arr);//o.instance.exports.m.buffer);
  let blob =new Blob([wave], { type: 'audio/wav' });
  let blobUrl = URL.createObjectURL(blob);
  audioEl.src = blobUrl;
  audioEl.style.zIndex=9999;
  audioEl.style.top = "1000px"
  audioEl.style.width = "800px";
  audioEl.style.position = "absolute";
  b.append(audioEl);
  audio = audioCtx.createMediaElementSource(audioEl);
  audioEl.controls = 1;
  //audioEl.play();
  
  audio.start = function () {
    audioEl.play();
  }
  audio.stop = function () {
    audioEl.pause();
  }
  audio.connect(analyser); 
  audioElem = audioEl;