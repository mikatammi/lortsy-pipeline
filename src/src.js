let canvas = document.createElement('canvas'),
b = document.body;

cursor_none 

wa();

lights_0;

canvas.ontouchend = //jos ei toimi puhelimessa touchend voi poistaa

document.onkeyup = () => {
  if (!audio){
    fps_counter0;
  /*
  function Generator(aa, bb, cc, dd) {
    let a = aa;
    let b = bb;
    let c = cc;
    let d = dd;
    return function () {
      a >>>= 0;
      b >>>= 0;
      c >>>= 0;
      d >>>= 0;
      let t = (a + b) | 0;
      a = b ^ (b >>> 9);
      b = (c + (c << 3)) | 0;
      c = (c << 21) | (c >>> 11);
      d = (d + 1) | 0;
      t = (t + d) | 0;
      c = (c + t) | 0;
      return (t >>> 0) / 4294967296;
    };
  }

  const random = Generator(29173, 11743, 40609, 91813);
*/
  let composeProgram = (gl,vertexShader, fragmentShader)=>{
    let vs = gl.createShader(gl_VERTEX_SHADER);
    gl.shaderSource(vs, vertexShader);
    gl.compileShader(vs);

    let fs = gl.createShader(gl_FRAGMENT_SHADER);
    gl.shaderSource(fs, fragmentShader);
    gl.compileShader(fs);

    let program = gl.createProgram();
    gl.attachShader(program, vs);
    gl.attachShader(program, fs);
    gl.linkProgram(program);


    shader_compile_debug;

    gl.bindBuffer(gl_ARRAY_BUFFER,  gl.createBuffer());
    gl.bufferData(gl_ARRAY_BUFFER, new Float32Array([-1, -1, 1, -1, -1, 1, 1, -1, -1, 1, 1, 1]), gl_STATIC_DRAW);

    gl.useProgram(program);
    gl.enableVertexAttribArray(0);
    gl.vertexAttribPointer(0, 2, gl_FLOAT, 0, 0, 0); 
    let tp =gl.getUniformLocation(program,'t')
    let eqp =gl.getUniformLocation(program,'b')
    return (t, eq)=>{
      gl.viewport(0, 0, canvas.width, canvas.height);
      gl.uniform1f(tp, t);
      gl.uniform4fv(eqp, eq);
      gl.drawArrays(gl_TRIANGLES,0,6);
    }
  }

  canvas.style.width = '100vw';
  canvas.style.height = '100vh';
  canvas.style.position = 'absolute';
  canvas.style.top = 0;
  canvas.style.left = 0;

shadersource
 
  let shader = composeProgram(
    canvas.getContext('webgl2'),
    `#version 300 es
in vec2 v;out vec2 s;void main(){s=v;gl_Position=vec4(v,0,1);}`
,
shader_glsl
  );

  full_screen


  ap();

  timing_exact_0;

  let refresh = () => {

    timing_exact_1
    fps_counter1;
    canvas.style.opacity = (t<bd-5?t:bd-t)/5;    //fade in canvas

    analyser.getByteFrequencyData(data);
    const dataIndexBasso = Math.floor((2.0 * 25.0 / 22050) * data.length);
    const dataIndexMedium = Math.floor((2.0 * 280.0 / 22050) * data.length);
    const dataIndexHigh = Math.floor((2.0 * 1500.0 / 22050) * data.length);
    const dataLow = data[dataIndexBasso]/255;
    const dataMedium = data[dataIndexMedium]/255;
    const dataHigh = data[dataIndexHigh]/255;

    shader(t, [dataLow, dataMedium, dataHigh,1]);
    lights_1;

    ending_0
    requestAnimationFrame(refresh);
  
    ending_1
     
  }
  refresh();
}
};
