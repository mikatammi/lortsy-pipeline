
  let bd, audio, o,audioCtx,analyser, data;
  timing_element_0
  let wa = async () =>{
    o = await WebAssembly.instantiate(
      f[1]
      , {m:Math});
      
      b.innerHTML = 'Ready Press enter!';
           
      b.append(canvas); 
  }

  let ap = () =>{
    
    let l= o.instance.exports.l/8;
    let s = o.instance.exports.s/4;
    let arr = new Float32Array(o.instance.exports.m.buffer);
    audioCtx = new AudioContext();
    audio = audioCtx.createBufferSource();
    let buffer =audioCtx.createBuffer(2,l,44100);

    let bu = buffer.getChannelData(0);
    let bu2 = buffer.getChannelData(1);

    for(let i=0; i< l; i++){
      bu[i]=arr[s+i*2];
      bu2[i]=arr[s+i*2+1];
    }
    //*/
    // set the buffer in the AudioBufferSourceNode
    audio.buffer = buffer;
    bd=audio.buffer.duration;
    
    analyser = new AnalyserNode(audioCtx);
    audio_controls
    
    
    analyser.fftSize = 2048;
    let bufferLength = analyser.frequencyBinCount;
    data = new Uint8Array(bufferLength);
    
    analyser.connect(audioCtx.destination);
    audio.start();
    //audio.loop = true;
    //audio.start();
  }
