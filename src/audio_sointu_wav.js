
  let bd, audio, audioCtx, o, audioEl, blobUrl, analyser,data;
  timing_element_0
  let wa = async () =>{
      audioEl = document.createElement("audio"); 
      var request = new XMLHttpRequest();
      request.open("GET", "/debug/audio.wav", true);
      request.responseType = "blob";    
      request.onload = function() {
        if (this.status == 200) {
          audioEl = new Audio(URL.createObjectURL(this.response));
          b.innerHTML = 'Ready Press enter!';
          b.append(canvas); 
        }
      }
      request.send();
  }

  let ap = () =>{
    audioCtx = new AudioContext();
    analyser = new AnalyserNode(audioCtx);
    audio_controls
   
    analyser.fftSize = 2048;
    let bufferLength = analyser.frequencyBinCount;
    data = new Uint8Array(bufferLength);
    
    analyser.connect(audioCtx.destination);
  }
