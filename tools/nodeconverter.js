let fs = require('fs'),
    binary = fs.readFileSync(process.argv[2]);
let b = new Uint8Array(binary.slice(0), x => x.charCodeAt(0));
process.stdout.write("[")
for(let i=0; i< b.length; i++){
    //process.stdout.write("0o");
    process.stdout.write(b[i].toString());
    if(i< b.length-1){
        process.stdout.write(",");
    }
}
process.stdout.write("]\n");
