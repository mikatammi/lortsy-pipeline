#!/bin/bash

mkdir -p intermediate

sed '/audio_controls/{
    s/audio_controls//g
    r src/no_audio_controls.js
}' src/audio_sointu.js > intermediate/audio_sointu_combined.js

cat intermediate/audio_sointu_combined.js src/gl_constants.js src/src.js > intermediate/src.packed_combined.js

./tools/sointu-compile -o intermediate/ -arch=wasm audio/song.yml

wat2wasm intermediate/song.wat -o intermediate/song.wasm

wasm-opt --enable-bulk-memory --enable-multivalue --strip-debug --disable-gc -O intermediate/song.wasm -o intermediate/song_optimized.wasm

# node tools/nodeconverter.js ./intermediate/song_optimized.wasm > intermediate/UintArrayOfWasm.js


# sed '/uint_wasm/{
#     s/uint_wasm//g
#     r intermediate/UintArrayOfWasm.js
# }' intermediate/src.packed_combined.js > intermediate/src.combined_with_wasm.js

mono tools/shader_minifier.exe -o intermediate/minified_shader.glsl -v --format js --preserve-externals src/shader.glsl
sed '/shader_compile_debug/{
    s/shader_compile_debug//g
    r src/empty.txt
}' intermediate/src.packed_combined.js > intermediate/src.packed_combined_strip_debug.js

sed '/fps_counter0/{
    s/fps_counter0//g
    r src/empty.txt
}' intermediate/src.packed_combined_strip_debug.js > intermediate/src.fps0.js

sed '/fps_counter1/{
    s/fps_counter1//g
    r src/empty.txt
}' intermediate/src.fps0.js > intermediate/src.fps1.js


sed '/full_screen/{
    s/full_screen//g
    r src/fullscreen.js
}' intermediate/src.fps1.js > intermediate/src.fullscreen.js

sed '/lights_0/{
    s/lights_0//g
    r src/lights_0.js
}' intermediate/src.fullscreen.js > intermediate/src.lights0.js

sed '/lights_1/{
    s/lights_1//g
    r src/lights_1.js
}' intermediate/src.lights0.js > intermediate/src.lights.js


sed '/timing_exact_0/{
    s/timing_exact_0//g
    r src/timing.exact.0.js
}' intermediate/src.lights.js > intermediate/src.fullscreen.timing.js

sed '/timing_element_0/{
    s/timing_element_0//g
    r src/empty.txt
}' intermediate/src.fullscreen.timing.js > intermediate/src.fullscreen.timing.element.js

sed '/timing_exact_1/{
    s/timing_exact_1//g
    r src/timing.exact.1.js
}' intermediate/src.fullscreen.timing.element.js > intermediate/src.fullscreen.timing.2.js

sed '/cursor_none/{
    s/cursor_none//g
    r src/cursor_none.js
}' intermediate/src.fullscreen.timing.2.js > intermediate/src.packed_combined_strip_debug_hidden_cursor.js

sed '/ending_0/{
    s/ending_0//g
    r src/end.0.js
}' intermediate/src.packed_combined_strip_debug_hidden_cursor.js > intermediate/src.ending.0.js

sed '/ending_1/{
    s/ending_1//g
    r src/end.1.js
}' intermediate/src.ending.0.js > intermediate/src.ending.js


sed '/shadersource/{
    s/shadersource//g
    r intermediate/minified_shader.glsl
}' intermediate/src.ending.js > intermediate/src.with_shader_combined.js

java -jar tools/closure-compiler-v20231112.jar  -O ADVANCED --language_in ECMASCRIPT_NEXT --language_out ECMASCRIPT_NEXT --assume_function_wrapper true --rewrite_polyfills false --js src/externs.js --js intermediate/src.with_shader_combined.js --js_output_file intermediate/demo.temp.js

ruby tools/pnginator.rb intermediate/demo.temp.js intermediate/song_optimized.wasm product/product.png.html

wc -c product/product.png.html
