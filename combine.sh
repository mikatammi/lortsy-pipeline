#!/bin/bash

mkdir -p intermediate

sed '/audio_controls/{
    s/audio_controls//g
    r src/audio_controls.js
}' src/audio_sointu.js > intermediate/audio_sointu_combined.js

cat intermediate/audio_sointu_combined.js src/gl_constants.js src/src.js > intermediate/src.combined_without_debug.js

./tools/sointu-compile -o intermediate/ -arch=wasm audio/song.yml

wat2wasm intermediate/song.wat -o intermediate/song.wasm

wasm-opt --enable-bulk-memory --enable-multivalue --strip-debug --disable-gc -O intermediate/song.wasm -o intermediate/song_optimized.wasm

node tools/nodeconverter.js ./intermediate/song_optimized.wasm > intermediate/UintArrayOfWasm.js

sed '/uint_wasm/{
    s/uint_wasm//g
    r intermediate/UintArrayOfWasm.js
}' intermediate/src.combined_without_debug.js > intermediate/src.combined_with_wasm.js

sed '/shader_compile_debug/{
    s/shader_compile_debug//g
    r src/shader_compiler_debug.js
}' intermediate/src.combined_with_wasm.js > intermediate/src.combined.js

sed '/cursor_none/{
    s/cursor_none//g
    r src/empty.txt
}' intermediate/src.combined.js > intermediate/src.combined.with.cursor.js

sed '/fps_counter0/{
    s/fps_counter0//g
    r src/fps0.js
}' intermediate/src.combined.with.cursor.js > intermediate/src.fps0.js

sed '/fps_counter1/{
    s/fps_counter1//g
    r src/fps1.js
}' intermediate/src.fps0.js > intermediate/src.fps1.js

sed '/full_screen/{
    s/full_screen//g
    r src/fullscreen.js
}' intermediate/src.fps1.js > intermediate/src.fullscreen.js

sed '/timing_exact_0/{
    s/timing_exact_0//g
    r src/empty.txt
}' intermediate/src.fullscreen.js > intermediate/src.fullscreen.timing.js

sed '/timing_element_0/{
    s/timing_element_0//g
    r src/timing.element.0.js
}' intermediate/src.fullscreen.timing.js > intermediate/src.fullscreen.timing.element.js

sed '/timing_exact_1/{
    s/timing_exact_1//g
    r src/timing.element.1.js
}' intermediate/src.fullscreen.timing.element.js > intermediate/src.fullscreen.timing.2.js

sed '/ending_0/{
    s/ending_0//g
    r src/empty.txt
}' intermediate/src.fullscreen.timing.2.js > intermediate/src.no.ending.0.js

sed '/ending_1/{
    s/ending_1//g
    r src/empty.txt
}' intermediate/src.no.ending.0.js > intermediate/src.no.ending.js

cat src/shader_begin.js src/shader.glsl src/shader_end.js > intermediate/shader_combined.js

sed '/shadersource/{
    s/shadersource//g
    r intermediate/shader_combined.js
}' intermediate/src.no.ending.js > debug/src.withshader.js


