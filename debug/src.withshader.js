
  let bd, audio, audioCtx, o, audioEl, blobUrl, analyser,data;
  
let audioElem;  let wa = async () =>{
      audioEl = document.createElement("audio"); 
      var request = new XMLHttpRequest();
      request.open("GET", "/debug/audio.wav", true);
      request.responseType = "blob";    
      request.onload = function() {
        if (this.status == 200) {
          audioEl = new Audio(URL.createObjectURL(this.response));
          b.innerHTML = 'Ready Press enter!';
          b.append(canvas); 
        }
      }
      request.send();
  }

  let ap = () =>{
    audioCtx = new AudioContext();
    analyser = new AnalyserNode(audioCtx);
    

audioEl.style.zIndex=9999;
audioEl.style.top = ""+(canvas.height-60)+"px"
audioEl.style.width = "800px";
audioEl.style.position = "absolute";
canvas.style.top = "0px";
canvas.style.left="0px";
b.append(audioEl);
audio = audioCtx.createMediaElementSource(audioEl);
audioEl.controls = 1;
//audioEl.play();
audioEl.loop.true;
bd = audioEl.duration;
audio.start = function () {
  audioEl.play();
}
audio.stop = function () {
  audioEl.pause();
}
audio.connect(analyser); 
audioElem = audioEl; 
   
    analyser.fftSize = 2048;
    let bufferLength = analyser.frequencyBinCount;
    data = new Uint8Array(bufferLength);
    
    analyser.connect(audioCtx.destination);
  }
let gl_ACTIVE_ATTRIBUTES = 35721;
let gl_ACTIVE_TEXTURE = 34016;
let gl_ACTIVE_UNIFORMS = 35718;
let gl_ALIASED_LINE_WIDTH_RANGE = 33902;
let gl_ALIASED_POINT_SIZE_RANGE = 33901;
let gl_ALPHA = 6406;
let gl_ALPHA_BITS = 3413;
let gl_ALWAYS = 519;
let gl_ARRAY_BUFFER = 34962;
let gl_ARRAY_BUFFER_BINDING = 34964;
let gl_ATTACHED_SHADERS = 35717;
let gl_BACK = 1029;
let gl_BLEND = 3042;
let gl_BLEND_COLOR = 32773;
let gl_BLEND_DST_ALPHA = 32970;
let gl_BLEND_DST_RGB = 32968;
let gl_BLEND_EQUATION = 32777;
let gl_BLEND_EQUATION_ALPHA = 34877;
let gl_BLEND_EQUATION_RGB = 32777;
let gl_BLEND_SRC_ALPHA = 32971;
let gl_BLEND_SRC_RGB = 32969;
let gl_BLUE_BITS = 3412;
let gl_BOOL = 35670;
let gl_BROWSER_DEFAULT_WEBGL = 37444;
let gl_BUFFER_SIZE = 34660;
let gl_BUFFER_USAGE = 34661;
let gl_BYTE = 5120;
let gl_CCW = 2305;
let gl_CLAMP_TO_EDGE = 33071;
let gl_COLOR_ATTACHMENT0 = 36064;
let gl_COLOR_BUFFER_BIT = 16384;
let gl_COLOR_CLEAR_VALUE = 3106;
let gl_COLOR_WRITEMASK = 3107;
let gl_COMPILE_STATUS = 35713;
let gl_COMPRESSED_TEXTURE_FORMATS = 34467;
let gl_CONSTANT_ALPHA = 32771;
let gl_CONSTANT_COLOR = 32769;
let gl_CONTEXT_LOST_WEBGL = 37442;
let gl_CULL_FACE = 2884;
let gl_CULL_FACE_MODE = 2885;
let gl_CURRENT_PROGRAM = 35725;
let gl_CURRENT_VERTEX_ATTRIB = 34342;
let gl_CW = 2304;
let gl_DECR = 7683;
let gl_DECR_WRAP = 34056;
let gl_DELETE_STATUS = 35712;
let gl_DEPTH_ATTACHMENT = 36096;
let gl_DEPTH_BITS = 3414;
let gl_DEPTH_BUFFER_BIT = 256;
let gl_DEPTH_CLEAR_VALUE = 2931;
let gl_DEPTH_COMPONENT = 6402;
let gl_DEPTH_FUNC = 2932;
let gl_DEPTH_RANGE = 2928;
let gl_DEPTH_STENCIL = 34041;
let gl_DEPTH_STENCIL_ATTACHMENT = 33306;
let gl_DEPTH_TEST = 2929;
let gl_DEPTH_WRITEMASK = 2930;
let gl_DITHER = 3024;
let gl_DONT_CARE = 4352;
let gl_DST_ALPHA = 772;
let gl_DST_COLOR = 774;
let gl_DYNAMIC_DRAW = 35048;
let gl_ELEMENT_ARRAY_BUFFER = 34963;
let gl_ELEMENT_ARRAY_BUFFER_BINDING = 34965;
let gl_EQUAL = 514;
let gl_FASTEST = 4353;
let gl_FLOAT = 5126;
let gl_FRAGMENT_SHADER = 35632;
let gl_FRAMEBUFFER = 36160;
let gl_FRAMEBUFFER_ATTACHMENT_OBJECT_NAME = 36049;
let gl_FRAMEBUFFER_ATTACHMENT_OBJECT_TYPE = 36048;
let gl_FRAMEBUFFER_ATTACHMENT_TEXTURE_CUBE_MAP_FACE = 36051;
let gl_FRAMEBUFFER_ATTACHMENT_TEXTURE_LEVEL = 36050;
let gl_FRAMEBUFFER_BINDING = 36006;
let gl_FRAMEBUFFER_COMPLETE = 36053;
let gl_FRAMEBUFFER_INCOMPLETE_ATTACHMENT = 36054;
let gl_FRAMEBUFFER_INCOMPLETE_DIMENSIONS = 36057;
let gl_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT = 36055;
let gl_FRAMEBUFFER_UNSUPPORTED = 36061;
let gl_FRONT = 1028;
let gl_FRONT_AND_BACK = 1032;
let gl_FRONT_FACE = 2886;
let gl_FUNC_ADD = 32774;
let gl_FUNC_REVERSE_SUBTRACT = 32779;
let gl_FUNC_SUBTRACT = 32778;
let gl_GENERATE_MIPMAP_HINT = 33170;
let gl_GEQUAL = 518;
let gl_GREATER = 516;
let gl_GREEN_BITS = 3411;
let gl_HIGH_FLOAT = 36338;
let gl_HIGH_INT = 36341;
let gl_IMPLEMENTATION_COLOR_READ_FORMAT = 35739;
let gl_IMPLEMENTATION_COLOR_READ_TYPE = 35738;
let gl_INCR = 7682;
let gl_INCR_WRAP = 34055;
let gl_INT = 5124;
let gl_INVALID_ENUM = 1280;
let gl_INVALID_FRAMEBUFFER_OPERATION = 1286;
let gl_INVALID_OPERATION = 1282;
let gl_INVALID_VALUE = 1281;
let gl_INVERT = 5386;
let gl_KEEP = 7680;
let gl_LEQUAL = 515;
let gl_LESS = 513;
let gl_LINEAR = 9729;
let gl_LINEAR_MIPMAP_LINEAR = 9987;
let gl_LINEAR_MIPMAP_NEAREST = 9985;
let gl_LINE_LOOP = 2;
let gl_LINES = 1;
let gl_LINE_STRIP = 3;
let gl_LINE_WIDTH = 2849;
let gl_LINK_STATUS = 35714;
let gl_LOW_FLOAT = 36336;
let gl_LOW_INT = 36339;
let gl_LUMINANCE = 6409;
let gl_LUMINANCE_ALPHA = 6410;
let gl_MAX_COMBINED_TEXTURE_IMAGE_UNITS = 35661;
let gl_MAX_CUBE_MAP_TEXTURE_SIZE = 34076;
let gl_MAX_FRAGMENT_UNIFORM_VECTORS = 36349;
let gl_MAX_RENDERBUFFER_SIZE = 34024;
let gl_MAX_TEXTURE_IMAGE_UNITS = 34930;
let gl_MAX_TEXTURE_SIZE = 3379;
let gl_MAX_VARYING_VECTORS = 36348;
let gl_MAX_VERTEX_ATTRIBS = 34921;
let gl_MAX_VERTEX_TEXTURE_IMAGE_UNITS = 35660;
let gl_MAX_VERTEX_UNIFORM_VECTORS = 36347;
let gl_MAX_VIEWPORT_DIMS = 3386;
let gl_MEDIUM_FLOAT = 36337;
let gl_MEDIUM_INT = 36340;
let gl_MIRRORED_REPEAT = 33648;
let gl_NEAREST = 9728;
let gl_NEAREST_MIPMAP_LINEAR = 9986;
let gl_NEAREST_MIPMAP_NEAREST = 9984;
let gl_NEVER = 512;
let gl_NICEST = 4354;
let gl_NO_ERROR = 0;
let gl_NONE = 0;
let gl_NOTEQUAL = 517;
let gl_ONE = 1;
let gl_ONE_MINUS_CONSTANT_ALPHA = 32772;
let gl_ONE_MINUS_CONSTANT_COLOR = 32770;
let gl_ONE_MINUS_DST_ALPHA = 773;
let gl_ONE_MINUS_DST_COLOR = 775;
let gl_ONE_MINUS_SRC_ALPHA = 771;
let gl_ONE_MINUS_SRC_COLOR = 769;
let gl_OUT_OF_MEMORY = 1285;
let gl_PACK_ALIGNMENT = 3333;
let gl_POINTS = 0;
let gl_POLYGON_OFFSET_FACTOR = 32824;
let gl_POLYGON_OFFSET_FILL = 32823;
let gl_POLYGON_OFFSET_UNITS = 10752;
let gl_RED_BITS = 3410;
let gl_RENDERBUFFER = 36161;
let gl_RENDERBUFFER_ALPHA_SIZE = 36179;
let gl_RENDERBUFFER_BINDING = 36007;
let gl_RENDERBUFFER_BLUE_SIZE = 36178;
let gl_RENDERBUFFER_DEPTH_SIZE = 36180;
let gl_RENDERBUFFER_GREEN_SIZE = 36177;
let gl_RENDERBUFFER_HEIGHT = 36163;
let gl_RENDERBUFFER_INTERNAL_FORMAT = 36164;
let gl_RENDERBUFFER_RED_SIZE = 36176;
let gl_RENDERBUFFER_STENCIL_SIZE = 36181;
let gl_RENDERBUFFER_WIDTH = 36162;
let gl_RENDERER = 7937;
let gl_REPEAT = 10497;
let gl_REPLACE = 7681;
let gl_RGB = 6407;
let gl_RGBA = 6408;
let gl_SAMPLE_ALPHA_TO_COVERAGE = 32926;
let gl_SAMPLE_BUFFERS = 32936;
let gl_SAMPLE_COVERAGE = 32928;
let gl_SAMPLE_COVERAGE_INVERT = 32939;
let gl_SAMPLE_COVERAGE_VALUE = 32938;
let gl_SAMPLER_CUBE = 35680;
let gl_SAMPLES = 32937;
let gl_SCISSOR_BOX = 3088;
let gl_SCISSOR_TEST = 3089;
let gl_SHADER_TYPE = 35663;
let gl_SHADING_LANGUAGE_VERSION = 35724;
let gl_SHORT = 5122;
let gl_SRC_ALPHA = 770;
let gl_SRC_ALPHA_SATURATE = 776;
let gl_SRC_COLOR = 768;
let gl_STATIC_DRAW = 35044;
let gl_STENCIL_ATTACHMENT = 36128;
let gl_STENCIL_BACK_FAIL = 34817;
let gl_STENCIL_BACK_FUNC = 34816;
let gl_STENCIL_BACK_PASS_DEPTH_FAIL = 34818;
let gl_STENCIL_BACK_PASS_DEPTH_PASS = 34819;
let gl_STENCIL_BACK_REF = 36003;
let gl_STENCIL_BACK_VALUE_MASK = 36004;
let gl_STENCIL_BACK_WRITEMASK = 36005;
let gl_STENCIL_BITS = 3415;
let gl_STENCIL_BUFFER_BIT = 1024;
let gl_STENCIL_CLEAR_VALUE = 2961;
let gl_STENCIL_FAIL = 2964;
let gl_STENCIL_FUNC = 2962;
let gl_STENCIL_PASS_DEPTH_FAIL = 2965;
let gl_STENCIL_PASS_DEPTH_PASS = 2966;
let gl_STENCIL_REF = 2967;
let gl_STENCIL_TEST = 2960;
let gl_STENCIL_VALUE_MASK = 2963;
let gl_STENCIL_WRITEMASK = 2968;
let gl_STREAM_DRAW = 35040;
let gl_SUBPIXEL_BITS = 3408;
let gl_TEXTURE = 5890;
let gl_TEXTURE0 = 33984;
let gl_TEXTURE_2D = 3553;
let gl_TEXTURE_BINDING_CUBE_MAP = 34068;
let gl_TEXTURE_CUBE_MAP = 34067;
let gl_TEXTURE_CUBE_MAP_NEGATIVE_X = 34070;
let gl_TEXTURE_CUBE_MAP_NEGATIVE_Y = 34072;
let gl_TEXTURE_CUBE_MAP_NEGATIVE_Z = 34074;
let gl_TEXTURE_CUBE_MAP_POSITIVE_X = 34069;
let gl_TEXTURE_CUBE_MAP_POSITIVE_Y = 34071;
let gl_TEXTURE_CUBE_MAP_POSITIVE_Z = 34073;
let gl_TEXTURE_MAG_FILTER = 10240;
let gl_TEXTURE_MIN_FILTER = 10241;
let gl_TEXTURE_WRAP_S = 10242;
let gl_TEXTURE_WRAP_T = 10243;
let gl_TRIANGLE_FAN = 6;
let gl_TRIANGLES = 4;
let gl_TRIANGLE_STRIP = 5;
let gl_UNPACK_ALIGNMENT = 3317;
let gl_UNPACK_COLORSPACE_CONVERSION_WEBGL = 37443;
let gl_UNPACK_FLIP_Y_WEBGL = 37440;
let gl_UNPACK_PREMULTIPLY_ALPHA_WEBGL = 37441;
let gl_UNSIGNED_BYTE = 5121;
let gl_UNSIGNED_INT = 5125;
let gl_UNSIGNED_SHORT = 5123;
let gl_VALIDATE_STATUS = 35715;
let gl_VENDOR = 7936;
let gl_VERSION = 7938;
let gl_VERTEX_ATTRIB_ARRAY_BUFFER_BINDING = 34975;
let gl_VERTEX_ATTRIB_ARRAY_ENABLED = 34338;
let gl_VERTEX_ATTRIB_ARRAY_NORMALIZED = 34922;
let gl_VERTEX_ATTRIB_ARRAY_POINTER = 34373;
let gl_VERTEX_ATTRIB_ARRAY_SIZE = 34339;
let gl_VERTEX_ATTRIB_ARRAY_STRIDE = 34340;
let gl_VERTEX_ATTRIB_ARRAY_TYPE = 34341;
let gl_VERTEX_SHADER = 35633;
let gl_VIEWPORT = 2978;
let gl_ZERO = 0;
let canvas = document.createElement('canvas'),
b = document.body;

 

wa();

;
let li = () =>{
    let host = "valot.instanssi";
    let port = 9910;
    let socket =new WebSocket("ws://"+host+":"+port);
    let tagi = "tmptknn";
    let lightCount = 27;
    /*
    var timeout = undefined;
    */
  
    socket.onclose = function (event) {
      //clearTimeout(timeout);
    }
    
    socket.onerror = function (event) {
      //console.log("error:", event);
    }

    socket.onmessage = function incoming(data) {
      //console.log("received:", data);
    };

    socket.onopen = function (event) {
 

        };

      


        let headerLength = 3 + tagi.length;
        let data = new Uint8Array(headerLength + 6 * lightCount);
        data[0] = 1;
        data[1] = 0;  // Nick tag

        let taglen = tagi.length;

        let counter = 0;
      

        return (eq)=>{
          counter++;
      
          for (let i = 0; i < taglen; i++) {
              data[i + 2] = tagi.charCodeAt(i);
          }
          data[2 + taglen] = 0;  // Nick tag end
          data[0] = 1;
          data[1] = 0;  // Nick tag
          for(let i = 0; i < lightCount; i++) {
              let p = headerLength + 6 * i; 
              data[p + 0] = 1; // Tehosteen tyyppi on yksi eli valo
              data[p + 1] = i; // Ensimmäinen valo löytyy indeksistä nolla
              data[p + 2] = 0; // Laajennustavu. Aina nolla.
              data[p + 3] = eq[0]; // Punainen
              data[p + 4] = eq[1]; // Vihreä
              data[p + 5] = eq[2]; // Sininen
          }
          if(counter%10 ==0) socket.send(data);
         };

      
    };

let lights = li();
canvas.ontouchend = //jos ei toimi puhelimessa touchend voi poistaa

document.onkeyup = () => {
  if (!audio){
    ;
let fpsCounter =document.createElement("p");
const times = [];
let fps;

fpsCounter.style.fontSize =20.0;
fpsCounter.style.color = "white";
fpsCounter.style.zIndex = 100;
fpsCounter.style.top = "20px";
fpsCounter.style.left = "300px";
fpsCounter.style.position = "absolute";
fpsCounter.innerHTML = "0 fps";
document.body.append(fpsCounter)  /*
  function Generator(aa, bb, cc, dd) {
    let a = aa;
    let b = bb;
    let c = cc;
    let d = dd;
    return function () {
      a >>>= 0;
      b >>>= 0;
      c >>>= 0;
      d >>>= 0;
      let t = (a + b) | 0;
      a = b ^ (b >>> 9);
      b = (c + (c << 3)) | 0;
      c = (c << 21) | (c >>> 11);
      d = (d + 1) | 0;
      t = (t + d) | 0;
      c = (c + t) | 0;
      return (t >>> 0) / 4294967296;
    };
  }

  const random = Generator(29173, 11743, 40609, 91813);
*/
  let composeProgram = (gl,vertexShader, fragmentShader)=>{
    let vs = gl.createShader(gl_VERTEX_SHADER);
    gl.shaderSource(vs, vertexShader);
    gl.compileShader(vs);

    let fs = gl.createShader(gl_FRAGMENT_SHADER);
    gl.shaderSource(fs, fragmentShader);
    gl.compileShader(fs);

    let program = gl.createProgram();
    gl.attachShader(program, vs);
    gl.attachShader(program, fs);
    gl.linkProgram(program);


    ;
if (!gl.getShaderParameter(vs, gl_COMPILE_STATUS)) {
    console.log(gl.getShaderInfoLog(vs)); // eslint-disable-line no-console
  }

if (!gl.getShaderParameter(fs, gl_COMPILE_STATUS)) {
    console.log(gl.getShaderInfoLog(fs)); // eslint-disable-line no-console
}

if (!gl.getProgramParameter(program, gl_LINK_STATUS)) {
    console.log(gl.getProgramInfoLog(program)); // eslint-disable-line no-console
}
    gl.bindBuffer(gl_ARRAY_BUFFER,  gl.createBuffer());
    gl.bufferData(gl_ARRAY_BUFFER, new Float32Array([-1, -1, 1, -1, -1, 1, 1, -1, -1, 1, 1, 1]), gl_STATIC_DRAW);

    gl.useProgram(program);
    gl.enableVertexAttribArray(0);
    gl.vertexAttribPointer(0, 2, gl_FLOAT, 0, 0, 0); 
    let tp =gl.getUniformLocation(program,'t')
    let eqp =gl.getUniformLocation(program,'b')
    return (t, eq)=>{
      gl.viewport(0, 0, canvas.width, canvas.height);
      gl.uniform1f(tp, t);
      gl.uniform4fv(eqp, eq);
      gl.drawArrays(gl_TRIANGLES,0,6);
    }
  }

  canvas.style.width = '100vw';
  canvas.style.height = '100vh';
  canvas.style.position = 'absolute';
  canvas.style.top = 0;
  canvas.style.left = 0;


var shader_glsl = `#version 300 es
precision highp float;
in vec2 s; // position x=[-1,1] y=[-1,1] window
out vec4 r; // result
uniform float t; //time
uniform vec4 b;  //equalizer values except .w=1


float sdSphere( vec3 p, float s )
{
  return length(p)-s;
}

float GetDist(vec3 p){
    return sdSphere(p, 0.5*(0.5+b.x*.5))+sin(b.x*40.*p.x)*sin(b.y*40.*p.y)*sin(b.z*40.*p.z)*0.2*b.z ;
}

float RayMarch(vec3 ro, vec3 rd) {
    float dO = 0.;
    
    for (int i = 0; i < 128; i++) {
        vec3 p = ro + rd * dO;
        float dS = GetDist(p);    
        dO += dS;
        
        if (dO > 10. || dS < 0.001) break;
    }
    
    return dO;
}

vec3 GetNormal(vec3 p) {

    vec2 e = vec2(.01, 0); 
    return normalize(GetDist(p) - vec3(
        GetDist(p-e.xyy),
        GetDist(p-e.yxy),
        GetDist(p-e.yyx)
    ));
}

vec4 draw(in vec2 uv){
    vec3 rd = normalize(vec3(uv.x, uv.y, 1));
    vec3 ro = vec3(0,0,-1);
    float d = RayMarch(ro, rd);
    vec3 p = ro + rd * d;
    vec3 n = GetNormal(p);
    vec3 l = normalize(vec3(-1,2,-3));
    float dif = clamp(dot(n, l), 0.3, 1.);

    vec3 col = d<3.?dif*vec3(0.7,0.6,0.5)+b.xyz:vec3(0);
    return vec4(col, 1);
}

void main() {
    vec2 uv = s*vec2(16./9.,1.);
    r=draw(uv);
 }`; 
  let shader = composeProgram(
    canvas.getContext('webgl2'),
    `#version 300 es
in vec2 v;out vec2 s;void main(){s=v;gl_Position=vec4(v,0,1);}`
,
shader_glsl
  );

  
canvas.width = window.innerWidth; // /4;//1920;  // dividing with 2 or 4 makes demo run much faster
canvas.height = window.innerHeight;// /4;//1080;  // but resolution is decimiced. hardcoded resolutions can also be used


  ap();

  ;

  let refresh = () => {

    
let t= audioElem.currentTime;    ;

const now = performance.now();
while (times.length > 0 && times[0] <= now - 1000) {
    times.shift();
}
times.push(now);
fps = times.length;

fpsCounter.innerHTML = ""+fps.toFixed(2)+" FPS";    canvas.style.opacity = (t<bd-5?t:bd-t)/5;    //fade in canvas

    analyser.getByteFrequencyData(data);
    const dataIndexBasso = Math.floor((2.0 * 25.0 / 22050) * data.length);
    const dataIndexMedium = Math.floor((2.0 * 280.0 / 22050) * data.length);
    const dataIndexHigh = Math.floor((2.0 * 1500.0 / 22050) * data.length);
    const dataLow = data[dataIndexBasso]/255;
    const dataMedium = data[dataIndexMedium]/255;
    const dataHigh = data[dataIndexHigh]/255;

    shader(t, [dataLow, dataMedium, dataHigh,1]);
    ;
lights([data[dataIndexBasso], data[dataIndexMedium], data[dataIndexHigh]]);
    
    requestAnimationFrame(refresh);
  
    
     
  }
  refresh();
}
};
